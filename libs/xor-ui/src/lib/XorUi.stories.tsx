import React from 'react';
import { XorUi, XorUiProps } from './XorUi';

export default {
  component: XorUi,
  title: 'XorUi',
};

export const primary = () => {
  /* eslint-disable-next-line */
  const props: XorUiProps = {};

  return <XorUi />;
};
