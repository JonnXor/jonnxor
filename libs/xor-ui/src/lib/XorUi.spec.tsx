import React from 'react';
import { render } from '@testing-library/react';

import XorUi from './XorUi';

describe('XorUi', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<XorUi />);
    expect(baseElement).toBeTruthy();
  });
});
