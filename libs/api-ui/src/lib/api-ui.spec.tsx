import React from 'react';
import { render } from '@testing-library/react';

import ApiUi from './api-ui';

describe('ApiUi', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<ApiUi />);
    expect(baseElement).toBeTruthy();
  });
});
