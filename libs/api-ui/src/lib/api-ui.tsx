import React from 'react';

import './api-ui.module.scss';

/* eslint-disable-next-line */
export interface ApiUiProps {}

export function ApiUi(props: ApiUiProps) {
  return (
    <div>
      <h1>Welcome to api-ui!</h1>
    </div>
  );
}

export default ApiUi;
