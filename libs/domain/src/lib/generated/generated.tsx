import { gql } from '@apollo/client';
import * as Apollo from '@apollo/client';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
};

/** TODO: Document this */
export type TodoEntity = {
  __typename?: 'TodoEntity';
  /** TODO: Document this */
  id: Scalars['String'];
  /** TODO: Document this */
  message: Scalars['String'];
  /** TODO: Document this */
  done: Scalars['Boolean'];
};

/** TODO: Document this */
export type UpdateTodoModel = {
  __typename?: 'UpdateTodoModel';
  /** TODO: Document this */
  id: Scalars['String'];
  /** TODO: Document this */
  message?: Maybe<Scalars['String']>;
  /** TODO: Document this */
  done?: Maybe<Scalars['Boolean']>;
};

/** TODO: Document this */
export type DeleteTodoModel = {
  __typename?: 'DeleteTodoModel';
  /** TODO: Document this */
  id?: Maybe<Scalars['String']>;
  /** TODO: Document this */
  message?: Maybe<Scalars['String']>;
  /** TODO: Document this */
  done?: Maybe<Scalars['Boolean']>;
};

export type Query = {
  __typename?: 'Query';
  /** TODO: Document this */
  todo: TodoEntity;
  /** TODO: Document this */
  todos: Array<TodoEntity>;
  /** TODO: Document this */
  todosById: Array<TodoEntity>;
};


export type QueryTodoArgs = {
  input: ReadTodoInput;
};


export type QueryTodosByIdArgs = {
  input: Array<ReadTodoInput>;
};

/** TODO: Document this */
export type ReadTodoInput = {
  /** TODO: Document this */
  id: Scalars['String'];
};

export type Mutation = {
  __typename?: 'Mutation';
  /** TODO: Document this */
  createTodo: TodoEntity;
  /** TODO: Document this */
  createTodos: Array<TodoEntity>;
  /** TODO: Document this */
  updateTodo: UpdateTodoModel;
  /** TODO: Document this */
  updateTodos: Array<UpdateTodoModel>;
  /** TODO: Document this */
  deleteTodo: DeleteTodoModel;
  /** TODO: Document this */
  deleteTodos: Array<DeleteTodoModel>;
};


export type MutationCreateTodoArgs = {
  input: CreateTodoInput;
};


export type MutationCreateTodosArgs = {
  input: Array<CreateTodoInput>;
};


export type MutationUpdateTodoArgs = {
  input: UpdateTodoInput;
};


export type MutationUpdateTodosArgs = {
  input: Array<UpdateTodoInput>;
};


export type MutationDeleteTodoArgs = {
  input: DeleteTodoInput;
};


export type MutationDeleteTodosArgs = {
  input: Array<DeleteTodoInput>;
};

/** TODO: Document this */
export type CreateTodoInput = {
  /** TODO: Document this */
  message: Scalars['String'];
};

/** TODO: Document this */
export type UpdateTodoInput = {
  /** TODO: Document this */
  id: Scalars['String'];
  /** TODO: Document this */
  message?: Maybe<Scalars['String']>;
  /** TODO: Document this */
  done?: Maybe<Scalars['Boolean']>;
};

/** TODO: Document this */
export type DeleteTodoInput = {
  /** TODO: Document this */
  id: Scalars['String'];
};

export type TodoEntityFragmentFragment = (
  { __typename?: 'TodoEntity' }
  & Pick<TodoEntity, 'id' | 'message' | 'done'>
);

export type UpdateTodoFragmentFragment = (
  { __typename?: 'UpdateTodoModel' }
  & Pick<UpdateTodoModel, 'id' | 'message' | 'done'>
);

export type DeleteTodoFragmentFragment = (
  { __typename?: 'DeleteTodoModel' }
  & Pick<DeleteTodoModel, 'id'>
);

export type CreateTodoMutationVariables = Exact<{
  input: CreateTodoInput;
}>;


export type CreateTodoMutation = (
  { __typename?: 'Mutation' }
  & { createTodo: (
    { __typename?: 'TodoEntity' }
    & TodoEntityFragmentFragment
  ) }
);

export type CreateTodosMutationVariables = Exact<{
  input: Array<CreateTodoInput> | CreateTodoInput;
}>;


export type CreateTodosMutation = (
  { __typename?: 'Mutation' }
  & { createTodos: Array<(
    { __typename?: 'TodoEntity' }
    & TodoEntityFragmentFragment
  )> }
);

export type ReadTodoQueryVariables = Exact<{
  input: ReadTodoInput;
}>;


export type ReadTodoQuery = (
  { __typename?: 'Query' }
  & { todo: (
    { __typename?: 'TodoEntity' }
    & TodoEntityFragmentFragment
  ) }
);

export type ReadTodosQueryVariables = Exact<{ [key: string]: never; }>;


export type ReadTodosQuery = (
  { __typename?: 'Query' }
  & { todos: Array<(
    { __typename?: 'TodoEntity' }
    & TodoEntityFragmentFragment
  )> }
);

export type ReadTodosByIdQueryVariables = Exact<{
  input: Array<ReadTodoInput> | ReadTodoInput;
}>;


export type ReadTodosByIdQuery = (
  { __typename?: 'Query' }
  & { todosById: Array<(
    { __typename?: 'TodoEntity' }
    & TodoEntityFragmentFragment
  )> }
);

export type UpdateTodoMutationVariables = Exact<{
  input: UpdateTodoInput;
}>;


export type UpdateTodoMutation = (
  { __typename?: 'Mutation' }
  & { updateTodo: (
    { __typename?: 'UpdateTodoModel' }
    & UpdateTodoFragmentFragment
  ) }
);

export type UpdateTodosMutationVariables = Exact<{
  input: Array<UpdateTodoInput> | UpdateTodoInput;
}>;


export type UpdateTodosMutation = (
  { __typename?: 'Mutation' }
  & { updateTodos: Array<(
    { __typename?: 'UpdateTodoModel' }
    & UpdateTodoFragmentFragment
  )> }
);

export type DeleteTodoMutationVariables = Exact<{
  input: DeleteTodoInput;
}>;


export type DeleteTodoMutation = (
  { __typename?: 'Mutation' }
  & { deleteTodo: (
    { __typename?: 'DeleteTodoModel' }
    & DeleteTodoFragmentFragment
  ) }
);

export type DeleteTodosMutationVariables = Exact<{
  input: Array<DeleteTodoInput> | DeleteTodoInput;
}>;


export type DeleteTodosMutation = (
  { __typename?: 'Mutation' }
  & { deleteTodos: Array<(
    { __typename?: 'DeleteTodoModel' }
    & DeleteTodoFragmentFragment
  )> }
);

export const TodoEntityFragmentFragmentDoc = gql`
    fragment todoEntityFragment on TodoEntity {
  id
  message
  done
}
    `;
export const UpdateTodoFragmentFragmentDoc = gql`
    fragment updateTodoFragment on UpdateTodoModel {
  id
  message
  done
}
    `;
export const DeleteTodoFragmentFragmentDoc = gql`
    fragment deleteTodoFragment on DeleteTodoModel {
  id
}
    `;
export const CreateTodoDocument = gql`
    mutation createTodo($input: CreateTodoInput!) {
  createTodo(input: $input) {
    ...todoEntityFragment
  }
}
    ${TodoEntityFragmentFragmentDoc}`;
export type CreateTodoMutationFn = Apollo.MutationFunction<CreateTodoMutation, CreateTodoMutationVariables>;

/**
 * __useCreateTodoMutation__
 *
 * To run a mutation, you first call `useCreateTodoMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateTodoMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createTodoMutation, { data, loading, error }] = useCreateTodoMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useCreateTodoMutation(baseOptions?: Apollo.MutationHookOptions<CreateTodoMutation, CreateTodoMutationVariables>) {
        return Apollo.useMutation<CreateTodoMutation, CreateTodoMutationVariables>(CreateTodoDocument, baseOptions);
      }
export type CreateTodoMutationHookResult = ReturnType<typeof useCreateTodoMutation>;
export type CreateTodoMutationResult = Apollo.MutationResult<CreateTodoMutation>;
export type CreateTodoMutationOptions = Apollo.BaseMutationOptions<CreateTodoMutation, CreateTodoMutationVariables>;
export const CreateTodosDocument = gql`
    mutation createTodos($input: [CreateTodoInput!]!) {
  createTodos(input: $input) {
    ...todoEntityFragment
  }
}
    ${TodoEntityFragmentFragmentDoc}`;
export type CreateTodosMutationFn = Apollo.MutationFunction<CreateTodosMutation, CreateTodosMutationVariables>;

/**
 * __useCreateTodosMutation__
 *
 * To run a mutation, you first call `useCreateTodosMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateTodosMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createTodosMutation, { data, loading, error }] = useCreateTodosMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useCreateTodosMutation(baseOptions?: Apollo.MutationHookOptions<CreateTodosMutation, CreateTodosMutationVariables>) {
        return Apollo.useMutation<CreateTodosMutation, CreateTodosMutationVariables>(CreateTodosDocument, baseOptions);
      }
export type CreateTodosMutationHookResult = ReturnType<typeof useCreateTodosMutation>;
export type CreateTodosMutationResult = Apollo.MutationResult<CreateTodosMutation>;
export type CreateTodosMutationOptions = Apollo.BaseMutationOptions<CreateTodosMutation, CreateTodosMutationVariables>;
export const ReadTodoDocument = gql`
    query readTodo($input: ReadTodoInput!) {
  todo(input: $input) {
    ...todoEntityFragment
  }
}
    ${TodoEntityFragmentFragmentDoc}`;

/**
 * __useReadTodoQuery__
 *
 * To run a query within a React component, call `useReadTodoQuery` and pass it any options that fit your needs.
 * When your component renders, `useReadTodoQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useReadTodoQuery({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useReadTodoQuery(baseOptions: Apollo.QueryHookOptions<ReadTodoQuery, ReadTodoQueryVariables>) {
        return Apollo.useQuery<ReadTodoQuery, ReadTodoQueryVariables>(ReadTodoDocument, baseOptions);
      }
export function useReadTodoLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<ReadTodoQuery, ReadTodoQueryVariables>) {
          return Apollo.useLazyQuery<ReadTodoQuery, ReadTodoQueryVariables>(ReadTodoDocument, baseOptions);
        }
export type ReadTodoQueryHookResult = ReturnType<typeof useReadTodoQuery>;
export type ReadTodoLazyQueryHookResult = ReturnType<typeof useReadTodoLazyQuery>;
export type ReadTodoQueryResult = Apollo.QueryResult<ReadTodoQuery, ReadTodoQueryVariables>;
export const ReadTodosDocument = gql`
    query readTodos {
  todos {
    ...todoEntityFragment
  }
}
    ${TodoEntityFragmentFragmentDoc}`;

/**
 * __useReadTodosQuery__
 *
 * To run a query within a React component, call `useReadTodosQuery` and pass it any options that fit your needs.
 * When your component renders, `useReadTodosQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useReadTodosQuery({
 *   variables: {
 *   },
 * });
 */
export function useReadTodosQuery(baseOptions?: Apollo.QueryHookOptions<ReadTodosQuery, ReadTodosQueryVariables>) {
        return Apollo.useQuery<ReadTodosQuery, ReadTodosQueryVariables>(ReadTodosDocument, baseOptions);
      }
export function useReadTodosLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<ReadTodosQuery, ReadTodosQueryVariables>) {
          return Apollo.useLazyQuery<ReadTodosQuery, ReadTodosQueryVariables>(ReadTodosDocument, baseOptions);
        }
export type ReadTodosQueryHookResult = ReturnType<typeof useReadTodosQuery>;
export type ReadTodosLazyQueryHookResult = ReturnType<typeof useReadTodosLazyQuery>;
export type ReadTodosQueryResult = Apollo.QueryResult<ReadTodosQuery, ReadTodosQueryVariables>;
export const ReadTodosByIdDocument = gql`
    query readTodosById($input: [ReadTodoInput!]!) {
  todosById(input: $input) {
    ...todoEntityFragment
  }
}
    ${TodoEntityFragmentFragmentDoc}`;

/**
 * __useReadTodosByIdQuery__
 *
 * To run a query within a React component, call `useReadTodosByIdQuery` and pass it any options that fit your needs.
 * When your component renders, `useReadTodosByIdQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useReadTodosByIdQuery({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useReadTodosByIdQuery(baseOptions: Apollo.QueryHookOptions<ReadTodosByIdQuery, ReadTodosByIdQueryVariables>) {
        return Apollo.useQuery<ReadTodosByIdQuery, ReadTodosByIdQueryVariables>(ReadTodosByIdDocument, baseOptions);
      }
export function useReadTodosByIdLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<ReadTodosByIdQuery, ReadTodosByIdQueryVariables>) {
          return Apollo.useLazyQuery<ReadTodosByIdQuery, ReadTodosByIdQueryVariables>(ReadTodosByIdDocument, baseOptions);
        }
export type ReadTodosByIdQueryHookResult = ReturnType<typeof useReadTodosByIdQuery>;
export type ReadTodosByIdLazyQueryHookResult = ReturnType<typeof useReadTodosByIdLazyQuery>;
export type ReadTodosByIdQueryResult = Apollo.QueryResult<ReadTodosByIdQuery, ReadTodosByIdQueryVariables>;
export const UpdateTodoDocument = gql`
    mutation updateTodo($input: UpdateTodoInput!) {
  updateTodo(input: $input) {
    ...updateTodoFragment
  }
}
    ${UpdateTodoFragmentFragmentDoc}`;
export type UpdateTodoMutationFn = Apollo.MutationFunction<UpdateTodoMutation, UpdateTodoMutationVariables>;

/**
 * __useUpdateTodoMutation__
 *
 * To run a mutation, you first call `useUpdateTodoMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateTodoMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateTodoMutation, { data, loading, error }] = useUpdateTodoMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useUpdateTodoMutation(baseOptions?: Apollo.MutationHookOptions<UpdateTodoMutation, UpdateTodoMutationVariables>) {
        return Apollo.useMutation<UpdateTodoMutation, UpdateTodoMutationVariables>(UpdateTodoDocument, baseOptions);
      }
export type UpdateTodoMutationHookResult = ReturnType<typeof useUpdateTodoMutation>;
export type UpdateTodoMutationResult = Apollo.MutationResult<UpdateTodoMutation>;
export type UpdateTodoMutationOptions = Apollo.BaseMutationOptions<UpdateTodoMutation, UpdateTodoMutationVariables>;
export const UpdateTodosDocument = gql`
    mutation updateTodos($input: [UpdateTodoInput!]!) {
  updateTodos(input: $input) {
    ...updateTodoFragment
  }
}
    ${UpdateTodoFragmentFragmentDoc}`;
export type UpdateTodosMutationFn = Apollo.MutationFunction<UpdateTodosMutation, UpdateTodosMutationVariables>;

/**
 * __useUpdateTodosMutation__
 *
 * To run a mutation, you first call `useUpdateTodosMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateTodosMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateTodosMutation, { data, loading, error }] = useUpdateTodosMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useUpdateTodosMutation(baseOptions?: Apollo.MutationHookOptions<UpdateTodosMutation, UpdateTodosMutationVariables>) {
        return Apollo.useMutation<UpdateTodosMutation, UpdateTodosMutationVariables>(UpdateTodosDocument, baseOptions);
      }
export type UpdateTodosMutationHookResult = ReturnType<typeof useUpdateTodosMutation>;
export type UpdateTodosMutationResult = Apollo.MutationResult<UpdateTodosMutation>;
export type UpdateTodosMutationOptions = Apollo.BaseMutationOptions<UpdateTodosMutation, UpdateTodosMutationVariables>;
export const DeleteTodoDocument = gql`
    mutation deleteTodo($input: DeleteTodoInput!) {
  deleteTodo(input: $input) {
    ...deleteTodoFragment
  }
}
    ${DeleteTodoFragmentFragmentDoc}`;
export type DeleteTodoMutationFn = Apollo.MutationFunction<DeleteTodoMutation, DeleteTodoMutationVariables>;

/**
 * __useDeleteTodoMutation__
 *
 * To run a mutation, you first call `useDeleteTodoMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteTodoMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteTodoMutation, { data, loading, error }] = useDeleteTodoMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useDeleteTodoMutation(baseOptions?: Apollo.MutationHookOptions<DeleteTodoMutation, DeleteTodoMutationVariables>) {
        return Apollo.useMutation<DeleteTodoMutation, DeleteTodoMutationVariables>(DeleteTodoDocument, baseOptions);
      }
export type DeleteTodoMutationHookResult = ReturnType<typeof useDeleteTodoMutation>;
export type DeleteTodoMutationResult = Apollo.MutationResult<DeleteTodoMutation>;
export type DeleteTodoMutationOptions = Apollo.BaseMutationOptions<DeleteTodoMutation, DeleteTodoMutationVariables>;
export const DeleteTodosDocument = gql`
    mutation deleteTodos($input: [DeleteTodoInput!]!) {
  deleteTodos(input: $input) {
    ...deleteTodoFragment
  }
}
    ${DeleteTodoFragmentFragmentDoc}`;
export type DeleteTodosMutationFn = Apollo.MutationFunction<DeleteTodosMutation, DeleteTodosMutationVariables>;

/**
 * __useDeleteTodosMutation__
 *
 * To run a mutation, you first call `useDeleteTodosMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteTodosMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteTodosMutation, { data, loading, error }] = useDeleteTodosMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useDeleteTodosMutation(baseOptions?: Apollo.MutationHookOptions<DeleteTodosMutation, DeleteTodosMutationVariables>) {
        return Apollo.useMutation<DeleteTodosMutation, DeleteTodosMutationVariables>(DeleteTodosDocument, baseOptions);
      }
export type DeleteTodosMutationHookResult = ReturnType<typeof useDeleteTodosMutation>;
export type DeleteTodosMutationResult = Apollo.MutationResult<DeleteTodosMutation>;
export type DeleteTodosMutationOptions = Apollo.BaseMutationOptions<DeleteTodosMutation, DeleteTodosMutationVariables>;
