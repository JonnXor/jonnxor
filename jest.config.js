module.exports = {
  projects: [
    '<rootDir>/apps/api',
    '<rootDir>/apps/web',
    '<rootDir>/apps/electron',
    '<rootDir>/libs/xor-ui',
    '<rootDir>/libs/data',
    '<rootDir>/libs/api-ui',
    '<rootDir>/libs/domain',
    '<rootDir>/apps/app',
    '<rootDir>/apps/app-electron',
  ],
};
