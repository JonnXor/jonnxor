import { ArgsType, Field } from '@nestjs/graphql';
import { FindOneOptions, FindManyOptions } from 'typeorm';

@ArgsType()
export class ReadOneOptionsArgs<Entity = unknown>
  implements FindOneOptions<Entity> {
  @Field({
    nullable: true,
    description: 'TODO: Document this',
  })
  withDeleted?: boolean;
}

@ArgsType()
export class ReadManyOptionsArgs<Entity = unknown>
  extends ReadOneOptionsArgs<Entity>
  implements FindManyOptions<Entity> {
  @Field({
    nullable: true,
    description: 'TODO: Document this',
  })
  skip?: number;

  @Field({
    nullable: true,
    description: 'TODO: Document this',
  })
  take?: number;
}
