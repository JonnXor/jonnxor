import { InjectRepository } from '@nestjs/typeorm';
import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';

import { User } from './entities/user.entity';

/** TODO: Document this */
@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>
  ) {}

  //#region CRUD

  //#region CREATE

  // TODO: ...

  //#endregion

  //#region READ

  // TODO: ...

  //#endregion

  //#region UPDATE

  // TODO: ...

  //#endregion

  //#region DELETE

  // TODO: ...

  //#endregion

  //#endregion

  // TODO: ...
}
