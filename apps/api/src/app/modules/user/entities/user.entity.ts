import { ObjectType, Field } from '@nestjs/graphql';
import { Exclude } from 'class-transformer';
import {
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
} from 'typeorm';

@ObjectType({ description: 'TODO: Document this' })
@Entity()
export class User {
  constructor(source?: Partial<User>) {
    Object.assign(this, source);
  }

  @Field({ name: 'id', description: 'TODO: Document this' })
  @PrimaryGeneratedColumn('uuid', { comment: 'TODO: Document this' })
  @Exclude()
  id: string;

  // TODO: ...

  @CreateDateColumn({ comment: 'TODO: Document this' })
  @Exclude()
  created: Date;

  @UpdateDateColumn({ comment: 'TODO: Document this' })
  @Exclude()
  updated: Date;

  @DeleteDateColumn({ nullable: true, comment: 'TODO: Document this' })
  @Exclude()
  deleted: Date;
}
