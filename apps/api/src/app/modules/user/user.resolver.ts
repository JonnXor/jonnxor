import { Inject, UseFilters } from '@nestjs/common';
import { Resolver } from '@nestjs/graphql';

import { UserService } from './user.service';
import { UserExceptionFilter } from './user.exception.filter';

import { User } from './entities/user.entity';

/** TODO: Document this */
@Resolver(() => User)
@UseFilters(UserExceptionFilter)
export class UserResolver {
  constructor(@Inject(UserService) private userService: UserService) {}

  //#region CRUD

  //#region CREATE

  // TODO: ...

  //#endregion

  //#region READ

  // TODO: ...

  //#endregion

  //#region UPDATE

  // TODO: ...

  //#endregion

  //#region DELETE

  // TODO: ...

  //#endregion

  //#endregion

  // TODO: ...
}
