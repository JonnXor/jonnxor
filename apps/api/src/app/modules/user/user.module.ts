import { Module } from '@nestjs/common';
import { APP_FILTER } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';

import { User } from './entities/user.entity';

import { UserService } from './user.service';
import { UserResolver } from './user.resolver';
import { UserTaskScheduler } from './user.task.scheduler';
import { UserExceptionFilter } from './user.exception.filter';

@Module({
  imports: [TypeOrmModule.forFeature([User])],
  providers: [
    UserService,
    UserResolver,
    UserTaskScheduler,
    {
      provide: APP_FILTER,
      useClass: UserExceptionFilter,
    },
  ],
  exports: [UserService],
})
export class UserModule {}
