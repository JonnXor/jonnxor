import { Inject, UseFilters } from '@nestjs/common';
import { Resolver } from '@nestjs/graphql';

import { AuthService } from './auth.service';
import { AuthExceptionFilter } from './auth.exception.filter';

import { User } from '../user/entities/user.entity';

/** TODO: Document this */
@Resolver(() => User)
@UseFilters(AuthExceptionFilter)
export class AuthResolver {
  constructor(@Inject(AuthService) private authService: AuthService) {}

  // TODO: ...
}
