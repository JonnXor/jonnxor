import { Module } from '@nestjs/common';
import { APP_FILTER } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';

import { User } from '../user/entities/user.entity';

import { AuthService } from './auth.service';
import { AuthResolver } from './auth.resolver';
import { AuthTaskScheduler } from './auth.task.scheduler';
import { AuthExceptionFilter } from './auth.exception.filter';

@Module({
  imports: [TypeOrmModule.forFeature([User])],
  providers: [
    AuthService,
    AuthResolver,
    AuthTaskScheduler,
    {
      provide: APP_FILTER,
      useClass: AuthExceptionFilter,
    },
  ],
  exports: [AuthService],
})
export class AuthModule {}
