import { InjectRepository } from '@nestjs/typeorm';
import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';

import { Board } from './entities/board.entity';

/** TODO: Document this */
@Injectable()
export class BoardTaskScheduler {
  constructor(
    @InjectRepository(Board)
    private boardRepository: Repository<Board>
  ) {}

  // TODO: ...
}
