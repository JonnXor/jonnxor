import { Inject, UseFilters } from '@nestjs/common';
import { Resolver } from '@nestjs/graphql';

import { BoardService } from './board.service';
import { BoardExceptionFilter } from './board.exception.filter';

import { Board } from './entities/board.entity';

/** TODO: Document this */
@Resolver(() => Board)
@UseFilters(BoardExceptionFilter)
export class BoardResolver {
  constructor(@Inject(BoardService) private boardService: BoardService) {}

  //#region CRUD

  //#region CREATE

  // TODO: ...

  //#endregion

  //#region READ

  // TODO: ...

  //#endregion

  //#region UPDATE

  // TODO: ...

  //#endregion

  //#region DELETE

  // TODO: ...

  //#endregion

  //#endregion

  // TODO: ...
}
