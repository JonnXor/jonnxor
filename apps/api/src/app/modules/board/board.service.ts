import { InjectRepository } from '@nestjs/typeorm';
import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';

import { Board } from './entities/board.entity';

/** TODO: Document this */
@Injectable()
export class BoardService {
  constructor(
    @InjectRepository(Board)
    private boardRepository: Repository<Board>
  ) {}

  //#region CRUD

  //#region CREATE

  // TODO: ...

  //#endregion

  //#region READ

  // TODO: ...

  //#endregion

  //#region UPDATE

  // TODO: ...

  //#endregion

  //#region DELETE

  // TODO: ...

  //#endregion

  //#endregion

  // TODO: ...
}
