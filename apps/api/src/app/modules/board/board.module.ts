import { Module } from '@nestjs/common';
import { APP_FILTER } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';

import { Board } from './entities/board.entity';

import { BoardService } from './board.service';
import { BoardResolver } from './board.resolver';
import { BoardTaskScheduler } from './board.task.scheduler';
import { BoardExceptionFilter } from './board.exception.filter';

import { ListModule } from './modules/list/list.module';

@Module({
  imports: [TypeOrmModule.forFeature([Board]), ListModule],
  providers: [
    BoardService,
    BoardResolver,
    BoardTaskScheduler,
    {
      provide: APP_FILTER,
      useClass: BoardExceptionFilter,
    },
  ],
  exports: [BoardService],
})
export class BoardModule {}
