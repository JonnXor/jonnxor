import { Module } from '@nestjs/common';
import { APP_FILTER } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';

import { List } from './entities/list.entity';

import { ListService } from './list.service';
import { ListResolver } from './list.resolver';
import { ListTaskScheduler } from './list.task.scheduler';
import { ListExceptionFilter } from './list.exception.filter';

import { CardModule } from './modules/card/card.module';

@Module({
  imports: [TypeOrmModule.forFeature([List]), CardModule],
  providers: [
    ListService,
    ListResolver,
    ListTaskScheduler,
    {
      provide: APP_FILTER,
      useClass: ListExceptionFilter,
    },
  ],
  exports: [ListService],
})
export class ListModule {}
