import { Module } from '@nestjs/common';
import { APP_FILTER } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';

import { Card } from './entities/card.entity';
import { CardService } from './card.service';
import { CardResolver } from './card.resolver';
import { CardTaskScheduler } from './card.task.scheduler';
import { CardExceptionFilter } from './card.exception.filter';

@Module({
  imports: [TypeOrmModule.forFeature([Card])],
  providers: [
    CardService,
    CardResolver,
    CardTaskScheduler,
    {
      provide: APP_FILTER,
      useClass: CardExceptionFilter,
    },
  ],
  exports: [CardService],
})
export class CardModule {}
