import { InjectRepository } from '@nestjs/typeorm';
import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';

import { List } from './entities/list.entity';

/** TODO: Document this */
@Injectable()
export class ListService {
  constructor(
    @InjectRepository(List)
    private listRepository: Repository<List>
  ) {}

  //#region CRUD

  //#region CREATE

  // TODO: ...

  //#endregion

  //#region READ

  // TODO: ...

  //#endregion

  //#region UPDATE

  // TODO: ...

  //#endregion

  //#region DELETE

  // TODO: ...

  //#endregion

  //#endregion

  // TODO: ...
}
