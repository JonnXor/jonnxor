import { Inject, UseFilters } from '@nestjs/common';
import { Resolver, Mutation, Query, Args } from '@nestjs/graphql';

//#region DTO

import {
  ReadOneOptionsArgs,
  ReadManyOptionsArgs,
} from '../../../../../../dto/read-options.dto';

//#region CRUD

import { ReadInput } from './dto/CRUD/read.dto';
import { UpdateInput } from './dto/CRUD/update.dto';

//#endregion

import { ArchiveInput } from './dto/archive.dto';
import { RestoreInput } from './dto/restore.dto';

//#endregion

import { Card } from './entities/card.entity';
import { CardService } from './card.service';
import { CardExceptionFilter } from './card.exception.filter';

/** Card Resolver */
@Resolver(() => Card)
@UseFilters(CardExceptionFilter)
export class CardResolver {
  constructor(@Inject(CardService) private cardService: CardService) {}

  //#region CRUD

  //#region CREATE

  @Mutation(() => Card, {
    description: 'CREATE one endpoint',
  })
  async createOne(): Promise<Card> {
    return this.cardService.create();
  }

  //#endregion

  //#region READ

  @Query(() => Card, {
    name: 'card',
    description: 'GET one endpoint',
  })
  async getOne(
    @Args({
      name: 'request',
      type: () => ReadInput,
      description: 'GET one request object',
    })
    request: ReadInput,
    @Args()
    params: ReadOneOptionsArgs<Card>
  ): Promise<Card> {
    return this.cardService.read(params, { ...request });
  }

  @Query(() => [Card], {
    name: 'cardsById',
    description: 'GET many endpoint',
  })
  async getMany(
    @Args({
      name: 'request',
      type: () => [ReadInput],
      description: 'GET many request object',
    })
    request: ReadInput[],
    @Args()
    params: ReadManyOptionsArgs<Card>
  ): Promise<Card[]> {
    return this.cardService.read(params, { ...request });
  }

  @Query(() => [Card], {
    name: 'cards',
    description: 'GET all endpoint',
  })
  async getAll(
    @Args()
    params: ReadManyOptionsArgs<Card>
  ): Promise<Card[]> {
    return this.cardService.read(params);
  }

  //#endregion

  //#region UPDATE

  @Mutation(() => Card, {
    name: 'updateOne',
    nullable: true,
    description: 'UPDATE one endpoint',
  })
  async putOne(
    @Args({
      name: 'request',
      type: () => UpdateInput,
      description: 'UPDATE one request object',
    })
    request: UpdateInput
  ): Promise<Partial<Card>> {
    return this.cardService.update({ ...request });
  }

  @Mutation(() => [Card], {
    name: 'updateMany',
    nullable: true,
    description: 'UPDATE many endpoint',
  })
  async putMany(
    @Args({
      name: 'request',
      type: () => [UpdateInput],
      description: 'UPDATE many request object',
    })
    request: UpdateInput[]
  ): Promise<Partial<Card>[]> {
    return this.cardService.update({ ...request });
  }

  @Mutation(() => [Card], {
    name: 'updateAll',
    nullable: true,
    description: 'UPDATE all endpoint',
  })
  async putAll(
    @Args({
      name: 'request',
      type: () => [UpdateInput],
      description: 'UPDATE all request object',
    })
    request: UpdateInput[]
  ): Promise<Partial<Card>[]> {
    return this.cardService.update({ ...request });
  }

  //#endregion

  //#region DELETE

  @Mutation(() => Card, {
    nullable: true,
    description: 'DELETE one endpoint',
  })
  async deleteOne(
    @Args({
      name: 'request',
      type: () => Card,
      description: 'DELETE one request object',
    })
    request: Card
  ): Promise<Card> {
    return this.cardService.delete({ ...request });
  }

  @Mutation(() => [Card], {
    nullable: true,
    description: 'DELETE many endpoint',
  })
  async deleteMany(
    @Args({
      name: 'request',
      type: () => [Card],
      description: 'DELETE many request object',
    })
    request: Card[]
  ): Promise<Card[]> {
    return this.cardService.delete({ ...request });
  }

  @Mutation(() => [Card], {
    nullable: true,
    description: 'DELETE all endpoint',
  })
  async deleteAll(): Promise<Card[]> {
    return this.cardService.delete();
  }

  //#endregion

  //#endregion

  @Mutation(() => Card, {
    nullable: true,
    description: 'Archive one endpoint',
  })
  async archiveOne(
    @Args({
      name: 'request',
      type: () => ArchiveInput,
      description: 'Archive one request object',
    })
    request: ArchiveInput
  ): Promise<Partial<Card>> {
    return this.cardService.archive({ ...request });
  }

  @Mutation(() => [Card], {
    nullable: true,
    description: 'Archive many endpoint',
  })
  async archiveMany(
    @Args({
      name: 'request',
      type: () => [ArchiveInput],
      description: 'Archive many request object',
    })
    request: ArchiveInput[]
  ): Promise<Partial<Card>[]> {
    return this.cardService.archive({ ...request });
  }

  @Mutation(() => [Card], {
    nullable: true,
    description: 'Archive all endpoint',
  })
  async archiveAll(): Promise<Partial<Card>[]> {
    return this.cardService.archive();
  }

  @Mutation(() => Card, {
    nullable: true,
    description: 'Restore one endpoint',
  })
  async restoreOne(
    @Args({
      name: 'request',
      type: () => RestoreInput,
      description: 'Restore one request object',
    })
    request: RestoreInput
  ): Promise<Partial<Card>> {
    return this.cardService.restore({ ...request });
  }

  @Mutation(() => [Card], {
    nullable: true,
    description: 'Restore many endpoint',
  })
  async restoreMany(
    @Args({
      name: 'request',
      type: () => [RestoreInput],
      description: 'Restore many request object',
    })
    request: RestoreInput[]
  ): Promise<Partial<Card>[]> {
    return this.cardService.restore({ ...request });
  }

  @Mutation(() => [Card], {
    nullable: true,
    description: 'Restore all endpoint',
  })
  async restoreAll(): Promise<Partial<Card>[]> {
    return this.cardService.restore();
  }
}
