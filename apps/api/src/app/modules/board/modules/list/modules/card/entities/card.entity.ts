import { ObjectType, Field } from '@nestjs/graphql';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
} from 'typeorm';
import { Exclude } from 'class-transformer';

@ObjectType({ description: 'Card Entity' })
@Entity('card')
export class Card {
  constructor(source?: Partial<Card>) {
    Object.assign(this, source);
  }

  @Field({ description: 'Unique identity field' })
  @PrimaryGeneratedColumn('uuid', { comment: 'Unique identity column' })
  @Exclude()
  id: string;

  @Field({ nullable: true, description: 'Card title field' })
  @Column({ nullable: true, comment: 'Card title column' })
  title: string;

  @Field({ nullable: true, description: 'Card markup field' })
  @Column({ nullable: true, comment: 'Card markup column' })
  markup: string;

  @CreateDateColumn({ comment: 'Created date column' })
  @Exclude()
  created: Date;

  @UpdateDateColumn({ comment: 'Updated date column' })
  @Exclude()
  updated: Date;

  @DeleteDateColumn({ nullable: true, comment: 'Archived date column' })
  @Exclude()
  archived: Date;
}
