import { Inject, UseFilters } from '@nestjs/common';
import { Resolver } from '@nestjs/graphql';

import { ListService } from './list.service';
import { ListExceptionFilter } from './list.exception.filter';

import { List } from './entities/list.entity';

/** TODO: Document this */
@Resolver(() => List)
@UseFilters(ListExceptionFilter)
export class ListResolver {
  constructor(@Inject(ListService) private listService: ListService) {}

  //#region CRUD

  //#region CREATE

  // TODO: ...

  //#endregion

  //#region READ

  // TODO: ...

  //#endregion

  //#region UPDATE

  // TODO: ...

  //#endregion

  //#region DELETE

  // TODO: ...

  //#endregion

  //#endregion

  // TODO: ...
}
