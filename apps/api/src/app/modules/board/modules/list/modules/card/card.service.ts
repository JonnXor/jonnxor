import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

//#region DTO

import {
  ReadOneOptionsArgs,
  ReadManyOptionsArgs,
} from '../../../../../../dto/read-options.dto';

//#endregion

import { Card } from './entities/card.entity';

/** Card Service */
@Injectable()
export class CardService {
  constructor(
    @InjectRepository(Card)
    private cardRepository: Repository<Card>
  ) {}

  //#region CRUD

  //#region CREATE

  /** CREATE one service */
  public async create(): Promise<Card>;

  /** CREATE service */
  public async create(): Promise<Card | Card[]> {
    return this.cardRepository.save(new Card());
  }

  //#endregion

  //#region READ

  /** READ one service */
  public async read(
    params: ReadOneOptionsArgs<Card>,
    request: Partial<Card>
  ): Promise<Card>;

  /** READ many service */
  public async read(
    params: ReadManyOptionsArgs<Card>,
    request: Partial<Card>[]
  ): Promise<Card[]>;

  /** READ all service */
  public async read(params: ReadManyOptionsArgs<Card>): Promise<Card[]>;

  /** READ service */
  public async read(
    params: ReadOneOptionsArgs<Card> | ReadManyOptionsArgs<Card>,
    request?: Partial<Card> | Partial<Card>[]
  ): Promise<Card | Card[]> {
    if (Array.isArray(request)) {
      const ids = request.map(({ id }) => id);
      return this.cardRepository.findByIds(ids, params);
    } else if (request?.id) {
      return this.cardRepository.findOne(request.id, params);
    }
    return this.cardRepository.find(params);
  }

  //#endregion

  //#region UPDATE

  /** UPDATE one service  */
  public async update(request: Partial<Card>): Promise<Partial<Card>>;

  /** UPDATE many service */
  public async update(request: Partial<Card>[]): Promise<Partial<Card>[]>;

  /** UPDATE all service */
  public async update(request: Partial<Card>): Promise<Partial<Card>[]>;

  /** UPDATE service */
  public async update(
    request: Partial<Card> | Partial<Card>[]
  ): Promise<Partial<Card> | Partial<Card>[]> {
    if (Array.isArray(request)) {
      const entities = request.map((entity) => {
        return new Card({ ...entity });
      });
      return this.cardRepository.save(entities);
    }
    const entity = new Card({ ...request });
    return this.cardRepository.save(entity);
  }

  //#endregion

  //#region DELETE

  /** DELETE one service */
  public async delete(request: Card): Promise<Card>;

  /** DELETE many service */
  public async delete(request: Card[]): Promise<Card[]>;

  /** DELETE all service */
  public async delete(): Promise<Card[]>;

  /** DELETE service */
  public async delete(request?: Card | Card[]): Promise<Card | Card[]> {
    if (Array.isArray(request)) {
      return this.cardRepository.remove(request);
    }
    return this.cardRepository.remove(request);
  }

  //#endregion

  //#endregion

  /** Archive one service */
  public async archive(request: Partial<Card>): Promise<Partial<Card>>;

  /** Archive many service */
  public async archive(request: Partial<Card>[]): Promise<Partial<Card>[]>;

  /** Archive all service */
  public async archive(): Promise<Partial<Card>[]>;

  /** Archive service */
  public async archive(
    request?: Partial<Card> | Partial<Card>[]
  ): Promise<Partial<Card> | Partial<Card>[]> {
    if (Array.isArray(request)) {
      return this.cardRepository.softRemove(request);
    }
    return this.cardRepository.softRemove(request);
  }

  /** Restore one service */
  public async restore(request: Partial<Card>): Promise<Partial<Card>>;

  /** Restore many service */
  public async restore(request: Partial<Card>[]): Promise<Partial<Card>[]>;

  /** Restore all service */
  public async restore(): Promise<Partial<Card>[]>;

  /** Restore service */
  public async restore(
    request?: Partial<Card> | Partial<Card>[]
  ): Promise<Partial<Card> | Partial<Card>[]> {
    if (Array.isArray(request)) {
      return this.cardRepository.recover(request);
    }
    return this.cardRepository.recover(request);
  }
}
