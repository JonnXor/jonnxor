import { InputType, PickType } from '@nestjs/graphql';

import { Card } from '../entities/card.entity';

@InputType({ description: 'Restore Request Object' })
export class RestoreInput extends PickType(Card, ['id'] as const, InputType) {}
