import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Cron } from '@nestjs/schedule';
import { Repository } from 'typeorm';

import { Card } from './entities/card.entity';

/** Card Task Scheduler */
@Injectable()
export class CardTaskScheduler {
  constructor(
    @InjectRepository(Card)
    private cardRepository: Repository<Card>
  ) {}

  @Cron('60 * * * * *')
  async emtpyRecycleBin() {
    console.log('Called when the current second is 60');
  }
}
