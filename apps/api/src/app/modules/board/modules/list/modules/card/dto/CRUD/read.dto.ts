import { InputType, PickType } from '@nestjs/graphql';

import { Card } from '../../entities/card.entity';

@InputType({ description: 'READ Request Object' })
export class ReadInput extends PickType(Card, ['id'] as const, InputType) {}
