import {
  InputType,
  IntersectionType,
  PickType,
  PartialType,
} from '@nestjs/graphql';

import { Card } from '../../entities/card.entity';

@InputType({ description: 'UPDATE Request Object' })
export class UpdateInput extends IntersectionType(
  PickType(Card, ['id'] as const, InputType),
  PartialType(PickType(Card, ['title', 'markup'] as const), InputType),
  InputType
) {}
