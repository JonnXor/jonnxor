import { InputType, PickType } from '@nestjs/graphql';

import { Card } from '../entities/card.entity';

@InputType({ description: 'Archive Request Object' })
export class ArchiveInput extends PickType(Card, ['id'] as const, InputType) {}
