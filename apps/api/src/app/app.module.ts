import { readFileSync } from 'fs';
import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ScheduleModule } from '@nestjs/schedule';

import { AuthModule } from './modules/auth/auth.module';
import { UserModule } from './modules/user/user.module';
import { BoardModule } from './modules/board/board.module';

import 'reflect-metadata';
@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: '35.242.149.218',
      port: 5432,

      username: 'jonnxor',
      password: '123456',
      ssl: {
        rejectUnauthorized: false,
        ca: readFileSync('C:/Users/JonnX/.ssl/server-ca.pem'),
        cert: readFileSync('C:/Users/JonnX/.ssl/client-cert.pem'),
        key: readFileSync('C:/Users/JonnX/.ssl/client-key.pem'),
      },

      database: 'www_jonnxor_is',

      synchronize: true,
      autoLoadEntities: true,
    }),
    GraphQLModule.forRoot({
      autoSchemaFile: 'apps/api/src/app/app.schema.gql',
    }),
    ScheduleModule.forRoot(),

    AuthModule,
    UserModule,
    BoardModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
